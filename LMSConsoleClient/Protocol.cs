﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.IO;

namespace LMS
{
    /*
     * Lobby, Check, Game, GameOver, Shoot
     * *payload size*
     * -=-
     * *data*
     * 
     * */
    class Protocol
    {
        public enum PayloadType
        {
            Lobby,
            MapData,
            PlayerData,
            Game,
            GameOver,
            Check,
            Success,
            Shoot,
            Lose,
            Victory,
            Error
        }
        private const string pattern = @"\b(Lobby|MapData|PlayerData|Game|GameOver|Check|Success|Shoot|Lose|Victory|Error)\b(\r\n|\n)[0-9]+(\r\n|\n)-=-(\r\n|\n)([a-zA-Z0-9-()]+(\s|\r\n|\n){0,1}){0,}";

        public static (PayloadType type, string data) ParseData(string message)
        {
            string type = "";
            PayloadType tOut = PayloadType.Error;
            char[] data;
            if (Regex.IsMatch(message, pattern))
            {
                StringReader ss = new StringReader(message);
                type = ss.ReadLine();
                foreach (PayloadType item in Enum.GetValues(typeof(PayloadType)))
                {
                    if (type == item.ToString())
                        tOut = item;
                }
                var dataSize = int.Parse(ss.ReadLine());
                data = new char[dataSize];
                ss.ReadLine();
                ss.Read(data, 0, dataSize);
                return (tOut, new string(data));
            }
            return (PayloadType.Error, "Wrong message");
        }

        public static string PackData(PayloadType type, string data)
        {
            StringBuilder output = new StringBuilder();
            data = data.Replace(" ", "\n");
            data = data.Replace("\r\n", "\n");
            data = data.Replace("\n\n", "\n");
            output.AppendLine(type.ToString());
            output.AppendLine(data.Length.ToString());
            output.AppendLine("-=-");
            if (data != null)
                output.Append(data);
            return output.ToString();
        }
    }
}
