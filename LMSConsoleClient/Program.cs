﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using LMS;
using System.Text.RegularExpressions;
using System.IO;
using System.Diagnostics;

namespace LMSConsoleClient
{
    class SocketHandler
    {
        private Socket socket;
        private const int bufSize = 1024;
        private byte[] buffer;

        public SocketHandler()
        {
            //int timeout = 60000; //1 min
            socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            buffer = new byte[bufSize];
            //socket.ReceiveTimeout = socket.SendTimeout = timeout;
        }

        public bool Connect(string ip)
        {
            try
            {
                socket.Connect(new IPEndPoint(IPAddress.Parse(ip), 25565));
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }

        public bool CheckConnection() => socket.Connected;
        public void Send(string message)
        {
            if (socket.Connected)
            {
                try
                {
                    buffer = Encoding.UTF8.GetBytes(message);
                    socket.Send(buffer, buffer.Length, SocketFlags.None);
                }
                catch (SocketException)
                {
                    Close();
                }
            }
        }
        public string Receive()
        {
            if (socket.Connected)
            {
                try
                {
                    buffer = new byte[bufSize];
                    socket.Receive(buffer, SocketFlags.None);
                    return Encoding.UTF8.GetString(buffer);
                }
                catch (SocketException)
                {
                    Close();
                }
            }
            return Protocol.PackData(Protocol.PayloadType.Error, "Connection timed out.");
        }

        public void Close()
        {
            socket.Shutdown(SocketShutdown.Both);
            socket.Close();
        }
    }
    class Client
    {
        class Player
        {
            int _posX = -1;
            int _posY = -1;
            bool alive = false;
            private Guid _guid;
            public int posX { get => _posX; }
            public int posY { get => _posY; }
            public Guid guid { get => _guid; }
            public Player(Guid guid)
            {
                _guid = guid;
                alive = true;
            }
            public void SetPos(int x, int y)
            {
                _posX = x;
                _posY = y;
            }
            public void Die()
            {
                alive = false;
            }
        }
        object playerDataLock = new object();
        enum State
        {
            None,
            Move,
            Shoot,
            Idle,
            Dead,
            Victory
        }
        SocketHandler socket;
        Guid clientGUID;
        List<Player> players = new List<Player>();
        private int[,] map;
        private int[,] mapWithPlayers;
        private int mapSize = 0;

        public Client()
        {
            socket = new SocketHandler();
        }

        public bool Connect(string ip)
        {
            return socket.Connect(ip);
        }

        public string getGUID() => clientGUID.ToString();
        public void Run()
        {
            //Game state
            var state = State.Idle;
            //Flag for waiting for player input
            bool waitingPlayerInput = false;
            //Flag to see, if the player data was updated
            bool updated = false;
            //Task to handle packets
            string shootWord = "";
            Task.Factory.StartNew(new Action(() =>
            {
                while (socket.CheckConnection())
                {
                    if (!waitingPlayerInput)
                    {
                        string str = socket.Receive();
                        (Protocol.PayloadType type, string data) = Protocol.ParseData(str);
                        if (type == Protocol.PayloadType.Lobby)
                        {
                            try
                            {
                                clientGUID = Guid.Parse(data);
                            }
                            catch (Exception e)
                            {
                                socket.Close();
                                break;
                            }
                        }
                        if (type == Protocol.PayloadType.Check)
                        {

                        }
                        if (type == Protocol.PayloadType.PlayerData)
                        {
                            lock (playerDataLock)
                            {
                                players.Clear();
                                StringReader sr = new StringReader(data);
                                var count = int.Parse(sr.ReadLine());
                                for (int i = 0; i < count; i++)
                                {
                                    try
                                    {
                                        var x = int.Parse(sr.ReadLine());
                                        var y = int.Parse(sr.ReadLine());
                                        Guid guid = Guid.Parse(sr.ReadLine().Replace("\n", "\0"));
                                        var p = new Player(guid);
                                        p.SetPos(x, y);
                                        players.Add(p);
                                    } catch(Exception)
                                    {
                                        socket.Close();
                                    }
                                }
                            }
                            updated = true;
                        }
                        if (type == Protocol.PayloadType.MapData)
                        {
                            Console.WriteLine("MAP");
                            StringReader sr = new StringReader(data);
                            try
                            {
                                mapSize = int.Parse(sr.ReadLine());
                                map = new int[mapSize, mapSize];
                                mapWithPlayers = new int[mapSize, mapSize];
                                for (int i = 0; i < mapSize; i++)
                                {
                                    for (int k = 0; k < mapSize; k++)
                                    {
                                        map[i, k] = int.Parse(sr.ReadLine());
                                    }
                                }
                            } catch (Exception)
                            {
                                socket.Close();
                            }
                        }
                        if (type == Protocol.PayloadType.Game)
                        {
                            state = State.Move;
                            waitingPlayerInput = true;
                            continue;
                        }
                        if (type == Protocol.PayloadType.Shoot)
                        {
                            state = State.Shoot;
                            shootWord = data;
                            waitingPlayerInput = true;
                            continue;
                        }
                        if (type == Protocol.PayloadType.Lose)
                        {
                            state = State.Dead;
                            waitingPlayerInput = true;
                            continue;
                        }
                        if (type == Protocol.PayloadType.Victory)
                        {
                            state = State.Victory;
                            waitingPlayerInput = true;
                            continue;
                        }
                        if (type == Protocol.PayloadType.Error)
                        {
                            socket.Close();
                        }
                        else
                        {
                            socket.Send(Protocol.PackData(Protocol.PayloadType.Success, ""));
                        }
                    }
                }
            }));
            while (true)
            {
                switch (state)
                {
                    case State.Idle:
                        if (updated)
                        {
                            Array.Copy(map, 0, mapWithPlayers, 0, mapSize * mapSize);
                            //bool flag = false;
                            lock (playerDataLock)
                            {
                                foreach (var player in players)
                                {
                                    if (player.guid == clientGUID) mapWithPlayers[player.posX, player.posY] = 2;
                                    else mapWithPlayers[player.posX, player.posY] = 3;
                                    //else Console.ForegroundColor = ConsoleColor.Red;
                                    //Console.Write("=");
                                    //Console.ForegroundColor = ConsoleColor.White;
                                    //flag = true;
                                    //break;
                                }
                            }
                            Console.Clear();
                            Console.ForegroundColor = ConsoleColor.White;
                            Console.WriteLine("Your GUID: {0}", clientGUID);
                            for (int i = 0; i < mapSize; i++)
                            {
                                for (int k = 0; k < mapSize; k++)
                                {
                                    if (mapWithPlayers[i, k] == 2) Console.ForegroundColor = ConsoleColor.Green;
                                    if (mapWithPlayers[i, k] == 3) Console.ForegroundColor = ConsoleColor.Red;
                                    if (mapWithPlayers[i, k] == 1) Console.ForegroundColor = ConsoleColor.DarkMagenta;
                                    var cur = mapWithPlayers[i, k];
                                    Console.Write(cur == 0 ? " " : cur == 1 ? "+" : cur == 2 ? "@" : "#");
                                    Console.ForegroundColor = ConsoleColor.White;
                                }
                                Console.WriteLine();
                            }
                            updated = false;
                        }
                        break;
                    case State.Move:
                        Console.WriteLine("Press q w e r d c x z a to move, any other to stay.");
                        while (Console.KeyAvailable)
                        {
                            Console.ReadKey(true);
                        }
                        var res = Console.ReadKey(true);
                        socket.Send(Protocol.PackData(Protocol.PayloadType.Game, res.KeyChar.ToString()));
                        Console.WriteLine("Waiting for other players.");
                        waitingPlayerInput = false;
                        state = State.Idle;
                        break;
                    case State.Shoot:
                        Console.WriteLine("Get ready to TYPE A WORD to shoot.");
                        while (Console.KeyAvailable)
                        {
                            Console.ReadKey(true);
                        }
                        Stopwatch timer = new Stopwatch();
                        for (int i = 3; i > 0; i--)
                        {
                            Console.WriteLine("Shoot in {0}", i);
                            System.Threading.Thread.Sleep(1000);
                        }
                        Console.WriteLine("TYPE THIS: {0}", shootWord);
                        timer.Start();
                        while(shootWord != Console.ReadLine())
                        {
                            Console.WriteLine("Wrong");
                        }
                        timer.Stop();
                        Console.WriteLine("Right");
                        Console.WriteLine("Your shooting time is {0}ms", timer.Elapsed.TotalMilliseconds.ToString());
                        socket.Send(Protocol.PackData(Protocol.PayloadType.Shoot, timer.Elapsed.TotalMilliseconds.ToString()));
                        waitingPlayerInput = false;
                        state = State.Idle;
                        break;
                    case State.Dead:
                        Console.WriteLine("Lol, you died. Press any key to exit.");
                        socket.Close();
                        return;
                    case State.Victory:
                        Console.WriteLine("You won. Press any key to exit.");
                        socket.Close();
                        return;
                    case State.None:
                        Console.WriteLine("Disconnected");
                        return;
                }
                if (!socket.CheckConnection()) state = State.None;
            }
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Client client = new Client();
            Console.WriteLine("Enter IP address or 'exit' to exit:");
            string ip;
            do
            {
                ip = Console.ReadLine();
                if (ip == "exit") return;
                try
                {
                    IPAddress.Parse(ip);
                    if (client.Connect(ip))
                    {
                        Console.WriteLine("Successfuly connected. Waiting for lobby to fill.");
                        client.Run();
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Server did not respond. Try another one.");
                    }
                }
                catch (FormatException)
                {
                    Console.WriteLine("Wrong ip. Try again.");
                }
            } while (true);
            
            Console.WriteLine("END");
            Console.ReadLine();
        }
    }
}
