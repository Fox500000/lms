﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;
namespace LMS
{
    class SocketHandler
    {
        private Socket socket;
        private const int bufSize = 1024;
        private byte[] buffer;

        public SocketHandler(Socket sock)
        {
            socket = sock;
            buffer = new byte[bufSize];
        }
        public bool CheckConnection() => socket.Connected;
        public void Send(string message)
        {
            if (socket.Connected)
            {
                try
                {
                    buffer = Encoding.UTF8.GetBytes(message);
                    socket.Send(buffer, buffer.Length, SocketFlags.None);
                }
                catch (SocketException)
                {
                    Close();
                }
            }
        }
        public string Receive()
        {
            if (socket.Connected)
            {
                try
                {
                    buffer = new byte[bufSize];
                    socket.Receive(buffer, SocketFlags.None);
                    return Encoding.UTF8.GetString(buffer);
                }
                catch (SocketException)
                {
                    Close();
                }
            }
            return Protocol.PackData(Protocol.PayloadType.Error, "Connection timed out.");
        }

        public void Close()
        {
            try
            {
                socket.Shutdown(SocketShutdown.Both);
            }
            catch (Exception)
            {
                Console.WriteLine("Socket already closed.");
            }
            socket.Close();
        }
    }
    class MyTCPSocket
    {
        private Socket serverSock;
        bool acceptingConnections;
        private List<(Guid id, SocketHandler sock)> connections = new List<(Guid id, SocketHandler sock)>();
        public MyTCPSocket()
        {
            serverSock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            IPHostEntry ipHost = Dns.GetHostEntry(Dns.GetHostName());
            IPAddress ip = null;
            foreach (IPAddress address in ipHost.AddressList)
            {
                if (address.AddressFamily == AddressFamily.InterNetwork)
                {
                    ip = address;
                    break;
                }
            }
            Console.WriteLine(ip.ToString());
            IPEndPoint p = new IPEndPoint(ip, 25565);
            serverSock.Bind(p);
            acceptingConnections = false;
        }

        public void Start()
        {
            serverSock.Listen(10);
            serverSock.BeginAccept(AcceptConnection, null);
        }
        public void BeginAccept()
        {
            acceptingConnections = true;
        }
        public void EndAccept() => acceptingConnections = false;
        public void Shutdown() => serverSock.Close();
        private void AcceptConnection(IAsyncResult obj)
        {

            Socket entry;
            try
            {
                entry = serverSock.EndAccept(obj);
                if (acceptingConnections)
                {
                    Guid guid = Guid.NewGuid();
                    var newEntry = new SocketHandler(entry);
                    newEntry.Send(Protocol.PackData(Protocol.PayloadType.Lobby, guid.ToString()));
                    var (type, data) = Protocol.ParseData(newEntry.Receive());
                    if (type == Protocol.PayloadType.Success)
                    {
                        if (newEntry.CheckConnection())
                        {
                            connections.Add((guid, newEntry));
                        }
                    }
                }
                else
                {
                    entry.Shutdown(SocketShutdown.Both);
                    entry.Close();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            serverSock.BeginAccept(AcceptConnection, null);
        }

        public int GetConnectionsCount() => connections.Count;
        public IReadOnlyList<(Guid id, SocketHandler sock)> GetConnections() => connections;

        public void SendToOne(string message, Guid guid)
        {
            var target = connections.Find(x => x.id == guid);
            if (target != default)
                target.sock.Send(message);
        }
        public void SendToAll(string message)
        {
            foreach (var item in connections)
            {
                item.sock.Send(message);
            }
        }

        public void CloseConnection(Guid guid)
        {
            var target = connections.Find(x => x.id == guid);
            if (target != default)
                target.sock.Close();
            connections.Remove(target);
        }
        public void CloseAllConnections()
        {
            foreach (var item in connections)
            {
                item.sock.Close();
            }
            connections.Clear();
        }
        public void ClearClosedConnections()
        {
            Console.WriteLine("Connections before wipe: {0}", connections.Count);
            connections.RemoveAll(x => !x.sock.CheckConnection());
            Console.WriteLine("Connections after wipe: {0}", connections.Count);
        }
    }
}
