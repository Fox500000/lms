﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using static LMS.Protocol;

namespace LMS
{

    class Player
    {
        int _posX = -1;
        int _posY = -1;
        Guid _guid;
        public int posX { get => _posX; }
        public int posY { get => _posY; }
        public Guid guid { get => _guid; }
        private bool _alive = false;
        public bool IsAlive { get => _alive; }
        public Player(Guid guid)
        {
            _guid = guid;
            _alive = true;
        }
        public void SetPos(int x, int y)
        {
            _posX = x;
            _posY = y;
        }
        public void Die()
        {
            _alive = false;
        }

        public static bool operator ==(Player left, Player other)
        {
            return left.guid == other.guid;
        }
        public static bool operator !=(Player left, Player other)
        {
            return left.guid != other.guid;
        }
    }
    class Game
    {
        int maxPlayers = 2;
        int mapSize = 10;
        enum GameState
        {
            Lobby,
            Game,
            GameOver,
            Reset
        }

        short[,] map;
        private List<Player> players = new List<Player>();
        private MyTCPSocket server;

        public Game(MyTCPSocket serv)
        {
            server = serv;
        }

        private string PackMap()
        {
            StringBuilder res = new StringBuilder();
            res.AppendLine(mapSize.ToString());
            for (int i = 0; i < mapSize; i++)
            {
                for (int j = 0; j < mapSize; j++)
                {
                    res.AppendLine(map[i, j].ToString());
                }
            }
            return res.ToString();
        }
        private string PackPlayerData()
        {
            StringBuilder res = new StringBuilder();
            res.AppendLine(players.Count.ToString());
            foreach (var item in players)
            {
                res.Append(item.posX.ToString() + " " + item.posY.ToString() + " " + item.guid.ToString() + '\n');
            }
            return res.ToString();
        }
        private void GenerateMap()
        {
            if (CheckAliveCount() <= 2) mapSize = 10;
            else mapSize = 5 * CheckAliveCount();
            map = new short[mapSize, mapSize];
            for (int i = 0; i < mapSize; i++)
            {
                for (int j = 0; j < mapSize; j++)
                {
                    if (j == 0 || i == 0 || j == mapSize - 1 || i == mapSize - 1)
                        map[i, j] = 1;
                }
            }
        }
        private void PlacePlayers()
        {
            foreach (var player in players)
            {
                int x = 0;
                int y = 0;
                var rand = new Random();
                bool check = false;
                do
                {
                    check = false;
                    x = rand.Next(0, mapSize);
                    y = rand.Next(0, mapSize);
                    foreach (var item in players)
                    {
                        if (x == item.posX && y == item.posY)
                        {
                            check = true;
                            break;
                        }
                    }
                    if (map[x, y] != 0) check = true;
                    if (!check)
                    {
                        player.SetPos(x, y);
                        break;
                    }
                } while (true);
            }
        }

        private int ParsePlayerInput(string data)
        {
            switch (data)
            {
                case "q": return 1;
                case "w": return 2;
                case "e": return 3;
                case "d": return 4;
                case "c": return 5;
                case "x": return 6;
                case "z": return 7;
                case "a": return 8;
                default: return 0;
            }
        }

        private void MovePlayer(Player player, int dir)
        {
            var playerX = player.posX;
            var playerY = player.posY;
            switch (dir)
            {
                case 1://q
                    if (playerX - 1 >= 0 && playerY - 1 >= 0 && map[playerX - 1, playerY - 1] == 0)
                    {
                        playerX--;
                        playerY--;
                    }
                    break;
                case 2://w
                    if (playerX - 1 >= 0 && map[playerX - 1, playerY] == 0)
                    {
                        playerX--;
                    }
                    break;
                case 3://e
                    if (playerX - 1 >= 0 && playerY + 1 < mapSize && map[playerX - 1, playerY + 1] == 0)
                    {
                        playerX--;
                        playerY++;
                    }
                    break;
                case 4://d
                    if (playerY + 1 < mapSize && map[playerX, playerY + 1] == 0)
                    {
                        playerY++;
                    }
                    break;
                case 5://c
                    if (playerX + 1 < mapSize && playerY + 1 < mapSize && map[playerX + 1, playerY + 1] == 0)
                    {
                        playerX++;
                        playerY++;
                    }
                    break;
                case 6://x
                    if (playerX + 1 < mapSize && map[playerX + 1, playerY] == 0)
                    {
                        playerX++;
                    }
                    break;
                case 7://z
                    if (playerX + 1 <= mapSize && playerY - 1 >= 0 && map[playerX + 1, playerY - 1] == 0)
                    {
                        playerX++;
                        playerY--;
                    }
                    break;
                case 8://a
                    if (playerY - 1 >= 0 && map[playerX, playerY - 1] == 0)
                    {
                        playerY--;
                    }
                    break;
                default: break;
            }
            player.SetPos(playerX, playerY);
        }

        private int CheckAliveCount()
        {
            int res = 0;
            foreach (var player in players)
            {
                if (player.IsAlive) ++res;
            }
            return res;
        }

        public string RandomString(int length, Random random)
        {
            const string chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
        //Clear players who are already disconnected
        private void ClearDisconnectedPlayers()
        {
            server.ClearClosedConnections();
            var connections = server.GetConnections();
            List<Player> delete = new List<Player>();
            foreach (var player in players)
            {
                if (!connections.Any(x => x.id == player.guid))
                    delete.Add(player);
            }
            foreach (var item in delete)
            {
                players.Remove(item);
            }
        }
        //Force disconnect
        private void DisconnectPlayers(List<Player> disconnected)
        {
            foreach (var player in disconnected)
            {
                Console.WriteLine("Player with GUID {0} disconnected.", player.guid);
                var conn = server.GetConnections().FirstOrDefault(x => x.id == player.guid);
                if (conn != default)
                {
                    server.CloseConnection(conn.id);
                }
                players.Remove(player);
            }
            disconnected.Clear();
        }
        public void Run()
        {
            server.Start();
            server.BeginAccept();
            var state = GameState.Lobby;
            bool running = true;
            bool generated = false;
            List<Player> disconnected = new List<Player>();
            Random random = new Random();
            while (running)
            {
                switch (state)
                {
                    case GameState.Reset:
                        #region Reset
                        Console.WriteLine("Resetting the server.");
                        server.CloseAllConnections();
                        server.BeginAccept();
                        generated = false;
                        state = GameState.Lobby;
                        Thread.Sleep(500);
                        break;
                    #endregion
                    case GameState.Lobby:
                        #region Lobby
                        Console.WriteLine("Starting lobby.");
                        Console.Write("Enter max player count: ");
                        if(!int.TryParse(Console.ReadLine(), out maxPlayers))
                        {
                            Console.WriteLine("Wrong format. Resetting to 2 players.");
                            maxPlayers = 2;
                        }
                        Thread.Sleep(500);
                        {
                            short c = 0;
                            while (server.GetConnectionsCount() < maxPlayers)
                            {
                                Console.Clear();
                                Console.WriteLine("Waiting for connections {0}", c == 0 ? "\\" : c == 1 ? "|" : c == 2 ? "/" : "-");
                                ++c;
                                if (c > 3) c = 0;
                                Console.WriteLine("------------------\nCurrent connections:");
                                foreach (var item in server.GetConnections())
                                {
                                    Console.WriteLine("GUID: {0}", item.id);
                                }
                                Thread.Sleep(600);
                            }
                        }
                        Console.WriteLine("Lobby filled.");
                        var connected = server.GetConnections();
                        for (int i = 0; i < maxPlayers && i < connected.Count; i++)
                        {
                            players.Add(new Player(connected[i].id));
                        }
                        for (int i = maxPlayers; i < connected.Count; i++)
                        {
                            connected[i].sock.Close();
                        }
                        server.EndAccept();
                        DisconnectPlayers(disconnected);
                        state = GameState.Game;
                        break;
                        #endregion
                    case GameState.Game:
                        #region Game
                        disconnected.Clear();
                        
                        Console.WriteLine("Entered game state.");
                        
                        if (!generated)
                        {
                            Console.WriteLine("Generating new data.");
                            generated = true;
                            GenerateMap();
                            PlacePlayers();
                            ClearDisconnectedPlayers();
                            var mapData = PackMap();
                            //Send map data
                            foreach (var player in players)
                            {
                                var conn = server.GetConnections().First(x => x.id == player.guid);
                                conn.sock.Send(PackData(PayloadType.MapData, mapData));
                                
                                if (ParseData(conn.sock.Receive()).type == PayloadType.Success)
                                    continue;
                                disconnected.Add(player);
                            }
                            Console.WriteLine("Current map:");
                            for(int i = 0; i < mapSize; i++)
                            {
                                for(int j = 0; j < mapSize; j++)
                                {
                                    Console.Write(map[i, j].ToString());
                                }
                                Console.WriteLine();
                            }
                        }
                        
                        DisconnectPlayers(disconnected);
                        
                        if (CheckAliveCount() <= 1) { state = GameState.GameOver; continue; }
                        
                        #region Connection check
                        Console.WriteLine("Checking client connections.");
                        foreach (var player in players)
                        {
                            var conn = server.GetConnections().FirstOrDefault(x => x.id == player.guid);
                            if (conn != default)
                            {
                                conn.sock.Send(PackData(PayloadType.Check, ""));
                                var (type, data) = ParseData(conn.sock.Receive());
                                if (type != PayloadType.Success) disconnected.Add(player);
                            }
                        }
                        DisconnectPlayers(disconnected);
                        #endregion
                        
                        if (CheckAliveCount() <= 1) { state = GameState.GameOver; continue; }
                        
                        Console.WriteLine("Sending player data to clients.");
                        var pData = PackPlayerData();

                        //Shooting check
                        #region Shooting step
                        Console.WriteLine("Starting shooting phase.");
                        foreach (var player in players)
                        {
                            if(player.IsAlive)
                            {
                                foreach (var otherPlayer in players)
                                {
                                    if (otherPlayer == player) continue;
                                    if(otherPlayer.IsAlive && (otherPlayer.posX == player.posX && otherPlayer.posY == player.posY))
                                    {
                                        string shootWord = RandomString(5, random);
                                        Console.WriteLine("Found dueling players:\nGUID p1: {0}\nGUID p2: {1}\n Shoot word: {2}", player.guid, otherPlayer.guid, shootWord);
                                        var conn = server.GetConnections().FirstOrDefault(x => x.id == player.guid);
                                        var otherConn = server.GetConnections().FirstOrDefault(x => x.id == otherPlayer.guid);
                                        if (conn != default && otherConn != default)
                                        {
                                            #region Send and check phase
                                            conn.sock.Send(PackData(PayloadType.Shoot, shootWord));
                                            otherConn.sock.Send(PackData(PayloadType.Shoot, shootWord));

                                            var (type, data) = ParseData(conn.sock.Receive());
                                            var (otherType, otherData) = ParseData(otherConn.sock.Receive());
                                            
                                            if (type != PayloadType.Shoot) { disconnected.Add(player); break; }
                                            if (otherType != PayloadType.Shoot) { disconnected.Add(otherPlayer); continue; }
                                            #endregion
                                            #region Compare phase
                                            double firstShot, secondShot;
                                            //Try parse data
                                            try
                                            {
                                                firstShot = double.Parse(data);
                                            } catch (FormatException)
                                            {
                                                disconnected.Add(player);
                                                Console.WriteLine("Player with GUID: {0}\nsent wrong format for shooting step.", player.guid.ToString());
                                                break;
                                            }
                                            try
                                            {
                                                secondShot = double.Parse(otherData);
                                            }
                                            catch (FormatException)
                                            {
                                                disconnected.Add(otherPlayer);
                                                Console.WriteLine("Player with GUID: {0}\nsent wrong format for shooting step.", otherPlayer.guid.ToString());
                                                continue;
                                            }
                                            if (firstShot < secondShot)
                                            {
                                                Console.WriteLine("Player with GUID {0} died.", otherPlayer.guid);
                                                otherPlayer.Die();
                                            }
                                            else if (firstShot > secondShot)
                                            {
                                                Console.WriteLine("Player with GUID {0} died.", player.guid);
                                                player.Die();
                                                break;
                                            }
                                            else continue;
                                            #endregion
                                        }
                                    }
                                }
                            }
                        }
                        foreach (var player in players)
                        {
                            if(!player.IsAlive)
                            {
                                var conn = server.GetConnections().FirstOrDefault(x => x.id == player.guid);
                                conn.sock.Send(PackData(PayloadType.Lose, ""));
                                disconnected.Add(player);
                            }
                        }
                        //DisconnectPlayers(disconnected);
                        #endregion
                        
                        //Send player data and move step
                        if (CheckAliveCount() <= 1) { state = GameState.GameOver; continue; }
                        
                        #region Player data and move
                        foreach (var player in players)
                        {
                            if (player.IsAlive)
                            {
                                var conn = server.GetConnections().FirstOrDefault(x => x.id == player.guid);
                                if (conn != default)
                                {
                                    conn.sock.Send(PackData(PayloadType.PlayerData, pData));
                                    if (ParseData(conn.sock.Receive()).type != PayloadType.Success)
                                    { disconnected.Add(player); continue; }

                                    conn.sock.Send(PackData(PayloadType.Game, ""));
                                }
                            }
                        }
                        Console.WriteLine("Entered input parsing");
                        foreach(var player in players)
                        {
                            if (player.IsAlive)
                            {
                                var conn = server.GetConnections().FirstOrDefault(x => x.id == player.guid);
                                if (conn != default)
                                {
                                    var res = ParseData(conn.sock.Receive());
                                    if (res.type == PayloadType.Game)
                                    {
                                        var input = ParsePlayerInput(res.data);
                                        MovePlayer(player, input);
                                    }
                                    if (res.type == PayloadType.Error) disconnected.Add(player);
                                }
                            }
                        }
                        #endregion
                        #endregion
                        break;
                    case GameState.GameOver:
                        #region GameOver
                        DisconnectPlayers(disconnected);
                        if(players.Count == 1)
                        {
                            var conn = server.GetConnections().FirstOrDefault(x => x.id == players[0].guid);
                            conn.sock.Send(PackData(PayloadType.Victory, ""));
                        }

                        foreach (var player in players)
                        {
                            disconnected.Add(player);
                        }
                        DisconnectPlayers(disconnected);
                        Console.WriteLine("END");
                        Console.WriteLine("What to do next:\n1. Reset\n2. Exit");
                        var result = Console.ReadLine();
                        if (result == "1") state = GameState.Reset;
                        if (result == "2") running = false;
                        
                        #endregion
                        break;
                }
            }

            foreach (var player in players)
            {
                disconnected.Add(player);
            }
            DisconnectPlayers(disconnected);
        }
    }
}
