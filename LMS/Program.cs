﻿using System;

namespace LMS
{

    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                MyTCPSocket socket = new MyTCPSocket();
                Game game = new Game(socket);
                game.Run();
            }
            catch (Exception)
            {
                Console.WriteLine("Server already up.");
                Console.ReadKey();
            }
        }
    }
}
